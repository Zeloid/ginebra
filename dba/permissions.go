package dba

import (
	"database/sql"
	"fmt"
	"os"
)

func GetPermissions(db *sql.DB, schema string, table string) map[string][]string {
	var grantee, grant string

	grants := make(map[string][]string)

	sql := `SELECT u.grantee, p.privilege_type
		FROM (
    SELECT 1 AS idx, 'SELECT' AS privilege_type
    UNION
    SELECT 2 AS idx, 'INSERT' AS privilege_type
    UNION
    SELECT 3 AS idx, 'UPDATE' AS privilege_type
    UNION
    SELECT 4 AS idx, 'DELETE' AS privilege_type
    UNION
    SELECT 5 AS idx, 'REFERENCES' AS privilege_type
    UNION
    SELECT 6 AS idx, 'TRIGGER' AS privilege_type) p
	CROSS JOIN (
    SELECT usename AS grantee FROM pg_catalog.pg_user
      UNION
    SELECT rolname AS grantee FROM pg_catalog.pg_roles
	) u
	WHERE has_table_privilege(u.grantee, concat($1::text, '.', $2::text)::regclass, p.privilege_type)
	ORDER BY grantee, idx`

	rows, err := db.Query(sql, schema, table)

	if err != nil {
		fmt.Printf("Unable to get table permissions: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {

		err = rows.Scan(&grantee, &grant)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}

		grants[grantee] = append(grants[grantee], grant)
	}

	return grants
}

