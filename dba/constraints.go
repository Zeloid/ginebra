package dba

import (
	"database/sql"
	"fmt"
	"os"
	"html/template"
)

type Constraint struct {
	Name    string
	Comment template.HTML
	Type    string
	Data    string
}

func GetTableConstrains(db *sql.DB, schema string, table string) []Constraint {
	var constraints []Constraint


	getTableForeignKeys(db, schema, table, &constraints)
	getTableCheckConstraints(db, schema, table, &constraints)


	return constraints
}

func getTableForeignKeys(db *sql.DB, schema string, table string, constraints *[]Constraint) {
	var curConstraint Constraint

	rows, err := db.Query(`
	SELECT 
		tc.constraint_name, 
		'FOREIGN KEY (' || string_agg(kcu.column_name, ', ') || ') => ' || ccu.table_schema || '.' ||ccu.table_name || '(' || string_agg(ccu.column_name, ', ') || ')' as data,
		tc.constraint_type
	FROM information_schema.table_constraints AS tc
	JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
	JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name
	WHERE constraint_type = 'FOREIGN KEY' AND tc.table_schema =$1 AND tc.table_name = $2
    GROUP BY tc.constraint_name, tc.constraint_type, ccu.table_schema, ccu.table_name`, schema, table)

	if err != nil {
		fmt.Printf("Unable to get table metadata: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {

		err = rows.Scan(&curConstraint.Name, &curConstraint.Data, &curConstraint.Type)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}

		*constraints = append(*constraints, curConstraint)
	}
}

func getTableCheckConstraints(db *sql.DB, schema string, table string, constraints *[]Constraint) {
	var curConstraint Constraint

	rows, err := db.Query(`
SELECT c.conname, pg_get_constraintdef(c.oid, true), tc.constraint_type
FROM information_schema.table_constraints tc
JOIN pg_constraint c ON c.conrelid = concat($1::text, '.', $2::text)::regclass AND conname=tc.constraint_name
WHERE c.contype='c' AND table_schema=$1 AND table_name=$2;`, schema, table)

	if err != nil {
		fmt.Printf("Unable to get table metadata: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {

		err = rows.Scan(&curConstraint.Name, &curConstraint.Data, &curConstraint.Type)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}

		*constraints = append(*constraints, curConstraint)
	}
}

