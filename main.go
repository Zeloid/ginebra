package main

import (
	"gitlab.com/Zeloid/ginebra.v1/dba"
	"gitlab.com/Zeloid/ginebra.v1/tmpl"
	"database/sql"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"html/template"
	"log"
	"net/http"
	"os"
	_ "github.com/lib/pq"
)

func main() {
	dbHost := flag.String("dbhost", "localhost", "Host address where PostgreSQL is running")
	dbPort := flag.Int("dbport", 5432, "Database port")
	dbUser := flag.String("dbuser", "postgres", "Database username")
	dbPass := flag.String("dbpassword", "", "Database password")
	dbName := flag.String("dbname", "postgres", "Database name")

	markdown := flag.Bool("markdown", false, "Print markdown and exit")
	confluenceMarkdown := flag.Bool("confluence", false, "if --markdown is present use confluence compatible markdown")

	port := flag.Int("port", 3000, "Listening port")
	host := flag.String("host", "", "Bind to host if present")
	tls := flag.Bool("tls", false, "Run in https mode")

	secret := flag.String("secret", "", "If this parameter is set the connection will be rejected"+
		" unless secret get parameter is sent with the same value")

	flag.Parse()

	if *markdown {
		createMarkdown(dbHost, dbPort, dbUser, dbPass, dbName, *confluenceMarkdown)
		os.Exit(0)
	}

	fmt.Printf("Connection String: host=%s port=%d user=%s password=%s dbname=%s sslmode=disable\n\n",
		*dbHost, *dbPort, *dbUser, *dbPass, *dbName)

	fmt.Printf("For help on how to change the default connection parameters use: %s -h\n\n", os.Args[0])

	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/", func(c *gin.Context) {
		var md Metadata

		if paramSecret := c.Request.URL.Query().Get("secret"); paramSecret != *secret {
			c.JSON(http.StatusUnauthorized, struct {
				Message string
			}{Message: "Unauthorized"})

			return
		}

		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable\n",
			*dbHost, *dbPort, *dbUser, *dbPass, *dbName)

		db, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			panic(err)
		}
		defer db.Close()

		err = db.Ping()
		if err != nil {
			panic(err)
		}

		fmt.Println("Successfully connected!")

		md.Tables = getTables(db)
		md.Enums = getEnumTypes(db)


		t, err := template.New("html").Parse(tmpl.HtmlTemplate)

		t.Execute(c.Writer, md)
	})

	fmt.Printf("Listening on %s:%d(HTTPS:%v)\n", *host, *port, *tls)

	addrString := fmt.Sprintf("%s:%d", *host, *port)
	if *tls {
		if err := r.RunTLS(addrString, "certs/server.pem", "certs/server.key"); err != nil {
			log.Panicf("Error: %v", err)
		}

	} else {
		if err := r.Run(addrString); err != nil {
			log.Panicf("Error: %v", err)
		}
	}
}

func createMarkdown(dbHost *string, dbPort *int, dbUser *string, dbPass *string, dbName *string, confluenceMode bool) {
		var md Metadata
		var err error
		var t *template.Template

		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable\n",
			*dbHost, *dbPort, *dbUser, *dbPass, *dbName)

		db, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			panic(err)
		}
		defer db.Close()

		err = db.Ping()
		if err != nil {
			panic(err)
		}

		md.Tables = getTables(db)
		md.Enums = getEnumTypes(db)

		if confluenceMode {
			t, err = template.New("markdown").Parse(tmpl.ConfluenceTemplate)
		} else {
			t, err = template.New("markdown").Parse(tmpl.MarkdownTemplate)
		}

		if err != nil {
			panic(err)
		}

		err = t.Execute(os.Stdout, md)
		if err != nil {
			panic(err)
		}
}


type ColumnMetadata struct {
	Name            string
	Comment         template.HTML
	Default         string
	IsNullable      bool
	UserDefinedType bool
	Type            string
	Length          int
}

type TableMetadata struct {
	Schema      string
	Name        string
	Comment     template.HTML
	Columns     []ColumnMetadata
	Grants      map[string][]string
	Constraints []dba.Constraint
}

type Metadata struct {
	Tables []TableMetadata
	Enums  map[string][]string
}

func getTables(db *sql.DB) []TableMetadata {
	var tables []TableMetadata
	var curTable TableMetadata

	rows, err := db.Query(
		`SELECT table_schema,table_name, coalesce(obj_description((table_schema||'.'||table_name)::regclass),'No description') AS comment 
			FROM information_schema.tables 
			WHERE table_type = 'BASE TABLE' 
			AND table_schema NOT IN ('pg_catalog', 'information_schema')
			ORDER BY comment, table_schema, table_name ASC`)

	if err != nil {
		fmt.Printf("Unable to get table metadata: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&curTable.Schema, &curTable.Name, &curTable.Comment)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}

		curTable.Columns = getColumns(db, curTable.Schema, curTable.Name)
		curTable.Grants = dba.GetPermissions(db, curTable.Schema, curTable.Name)
		curTable.Constraints = dba.GetTableConstrains(db, curTable.Schema, curTable.Name)

		tables = append(tables, curTable)
	}

	return tables
}

func getColumns(db *sql.DB, schema string, table string) []ColumnMetadata {
	var columns []ColumnMetadata
	var curColumn ColumnMetadata

	rows, err := db.Query(
		`SELECT column_name, coalesce(column_default, ''), is_nullable='YES', 
			CASE data_type WHEN 'USER-DEFINED' THEN udt_schema||'.'||udt_name ELSE udt_name END, 
			coalesce(character_maximum_length::int, 0), 
			coalesce(col_description(($1||'.'||$2)::regclass, ordinal_position::int), ''), data_type='USER-DEFINED' 
			FROM information_schema.columns where table_schema = $1 AND table_name = $2 
			ORDER BY ordinal_position`, schema, table)

	if err != nil {
		fmt.Printf("Unable to get columns metadata: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(
			&curColumn.Name,
			&curColumn.Default,
			&curColumn.IsNullable,
			&curColumn.Type,
			&curColumn.Length,
			&curColumn.Comment,
			&curColumn.UserDefinedType)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}
		columns = append(columns, curColumn)
	}

	return columns
}


func getEnumTypes(db *sql.DB) map[string][]string {
	var typeName, element string

	enumTypes := make(map[string][]string)

	rows, err := db.Query(
		`SELECT n.nspname || '.' || t.typname AS name, e.enumlabel AS element 
			FROM pg_type t JOIN pg_enum e ON e.enumtypid = t.oid 
			JOIN pg_namespace n ON n.oid = t.typnamespace 
			ORDER BY e.enumtypid, e.enumsortorder`)

	if err != nil {
		fmt.Printf("Unable to get enum types: %s\n", err)
		os.Exit(1)
	}

	defer rows.Close()

	for rows.Next() {

		err = rows.Scan(&typeName, &element)

		if err != nil {
			fmt.Printf("Unable to get table metadata: %s\n", err)
			os.Exit(1)
		}

		enumTypes[typeName] = append(enumTypes[typeName], element)
	}

	return enumTypes
}

