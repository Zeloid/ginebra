FROM ubuntu:latest

ADD ginebra .
ADD start.sh .
ADD tmpl templates

RUN chmod +x ginebra start.sh

CMD ./start.sh