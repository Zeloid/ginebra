#!/bin/sh
DB_HOST=localhost
DB_NAME=testdb
DB_PASSWORD=1234
DB_PORT=5432
DB_USER=testmaster
PORT=1234

./ginebra -dbhost ${DB_HOST} -dbname ${DB_NAME} -dbpassword ${DB_PASSWORD} -dbport ${DB_PORT} -dbuser ${DB_USER} -port ${PORT} $*
# -secret ${SECRET}
