#!/usr/bin/env bash

docker build . -t ginebra
docker run -it --rm \
    --env DB_HOST=localhost \
    --env DB_NAME=master-data-db \
    --env DB_PASSWORD=1234 \
    --env DB_PASSWORD=5432 \
    --env DB_USER=master-data-user \
    --env PORT=1234 \
    --env SECRET=skdblaewabdalbd \
    ginebra $*
