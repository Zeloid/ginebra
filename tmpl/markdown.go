package tmpl

const MarkdownTemplate = `DB Docs
=======

# Tables
{{ with .Tables}}
    {{ range . }}
        {{ $schema := .Schema}}
        {{ $table := .Name}}
## {{.Schema}}.{{.Name}}
_{{ .Comment }}_

#

|Column|Type|Length|Nullable?|Default|Comment|
|------|----|------|---------|-------|-------|
{{ range .Columns }}|{{.Name}}|{{.Type}}|{{.Length}}|{{if .IsNullable}}Yes{{else}}No{{end}}|{{.Default}}|{{.Comment}}|
{{ end }}

#

{{ with .Grants }}
|User/Role|Permissions|
|---------|-----------|
{{ range $grantee, $grants := . }}|{{ $grantee }}|{{ range $index, $element := $grants }}{{if $index}}, {{end}}{{ . }}{{ end }}|
{{ end }}
{{ end }}

#

{{ with .Constraints }}
|Name|Constraint definition|
|---------|-----------|
{{ range $index, $const := . }}|{{ .Name }}|{{ .Data }}|
{{ end }}
{{ end }}
{{ end }}
{{ end }}

#

{{ with .Enums}}
# Enum Types
{{ range $type, $elements := . }}
{{ $type }}: {{ range $elements }}{{ . }},{{ end }}
{{ end }}
{{ end }}
`