package tmpl

const HtmlTemplate = `<html>
<head>
    <title>DB Docs</title>
    <style>
        .table-container {
            margin-bottom: 20px;
        }

        .table-container table .caption {
            background: -webkit-linear-gradient(left, rgba(0, 75, 150, 1)  0%,rgba(0, 75, 150, 1)  40%, rgba(255, 255, 255, 1) 100%);
            font-weight: bold;
            color: #eeeeee;
            font-size: 1.3em;
            padding: 10px;
        }

        .table-container table .comment {
            font-style: italic;
            color: #333333;
        }

        .table-container table td, .table-container table th {
            border: thin solid #333333;
            padding: 3px;
        }

        .table-container table {
            border-collapse: collapse;
        }

        table .title {
            font-weight: bold;
            font-style: italic;
            font-size: 3em;
            margin-bottom: 1em;
            text-align: center;
            background: -webkit-linear-gradient(left, rgba(0, 75, 150, 1) 0%, rgba(0, 50, 100, 1) 15%,rgba(0, 50, 100, 1) 85%, rgba(0,  75, 150, 1) 100%);
            color: #ffffff;
        }

        table .subtitle {
            font-weight: bold;
            font-size: 1.25em;
            margin-bottom: 0.5em;
        }

        .grants .role, .enumType .enumName {
            display: inline-block;
            min-width: 220px;
            font-weight: bold;
        }

        .grants span.grant:after, .enumType span.enumElement:after {
            content: ", ";
            font-weight: bold;
            background-color: white !important;
        }

        .grants span.grant:hover, .enumType span.enumElement:hover {
            background-color: #dddddd;
        }

        .grants span.grant:last-child::after, .enumType span.enumElement:last-child::after {
            content: "";
        }

        .table-container {
            width: 80%;
            margin: auto;
        }

        .table-container table {
            width: 100%;
        }

        tr.hidden {
            display: none;
        }

        .controlBar {
            margin-bottom: 5px;
            background: -webkit-linear-gradient(left, rgba(0, 50, 100, 1) 0%, rgba(0, 50, 100, 1) 70%, rgba(255, 255, 255, 1) 100%);
            background-color: #004422;
            padding: 4px;
        }

        .controlBar INPUT {
            border: thin solid black;
            font-size: 2em;
        }

        .sep {
            border: none !important;
        }
    </style>
</head>
<body>
<div class="table-container">
    <div class="controlBar">
        <input type="text" id="search-field" placeholder="Filter" />
    </div>
    <table>
        <tr>
            <td colspan="6" class="title">Tables</td>
        </tr>
        <tr>
            <td  class="sep" colspan="6">&nbsp;</td>
        </tr>
    {{ with .Tables}}
    {{ range . }}
        {{ $schema := .Schema}}
        {{ $table := .Name}}
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6" class="caption"><a name="table_{{.Schema}}.{{.Name}}"></a>{{.Schema}}.{{.Name}}</td>
        </tr>
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6" class="comment">{{ .Comment }}</td>
        </tr>
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <th>Column</th>
            <th>Type</th>
            <th>Length</th>
            <th>Nullable?</th>
            <th>Default</th>
            <th>Comment</th>
        </tr>
    {{ range .Columns }}

        <tr data-owner="{{ $schema }}.{{ $table }}">
            <td>{{.Name}}</td>
            <td>{{if .UserDefinedType }}
            <a href="#enumtype_{{.Type}}">{{.Type}}</a>
            {{ else }}
                {{.Type}}
            {{ end }}</td>
            <td>{{.Length}}</td>
            <td>{{if .IsNullable}}Yes{{else}}No{{end}}</td>
            <td>{{.Default}}</td>
            <td>{{.Comment}}</td>
        </tr>

    {{ end }}
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6">&nbsp;</td>
        </tr>
    {{ with .Grants }}
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6" class="grants">
                <div class="subtitle">Permissions by role:</div>
            {{ range $grantee, $grants := . }}
                <div>
                    <span class="role">{{ $grantee }}</span>:
                {{ range $grants }}
                    <span class="grant">{{ . }}</span>
                {{ end }}
                </div>
            {{ end }}
            </td>
        </tr>
    {{ end }}
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6">&nbsp;</td>
        </tr>
    {{ with .Constraints }}
        <tr data-owner="{{ $schema }}.{{ $table }}">
            <td colspan="6" class="constraints">
                <div class="subtitle">Constraints:</div>
            {{ range . }}
                <div>
                    <span title="{{ .Name }}" class="constraintName">{{ .Data }}</span>
                </div>
            {{ end }}
            </td>
        </tr>
    {{ end }}
        <tr data-owner="{{.Schema}}.{{.Name}}">
            <td colspan="6" class="sep">&nbsp;</td>
        </tr>
    {{ end }}
    {{ end }}
        <tr>
            <td colspan="6" class="sep">&nbsp;</td>
        </tr>
    {{ with .Enums}}
        <tr>
            <td colspan="6" class="title">Enum Types</td>
        </tr>
    {{ range $type, $elements := . }}
        <tr data-owner="{{ $type }}">
            <td colspan="6" class="enumType">
                <div>
                    <a name="enumtype_{{ $type }}"></a>
                    <span class="enumName">{{ $type }}</span>:
                {{ range $elements }}
                    <span class="enumElement">{{ . }}</span>
                {{ end }}
                </div>
            </td>
        </tr>
    {{ end }}
    {{ end }}
    </table>

</div>
<script type="application/javascript">
    document.getElementById("search-field").addEventListener("keyup", function() {

        let searchText = document.getElementById("search-field").value;
        let trs = document.querySelectorAll('tr[data-owner]')
        for(let i = 0; i < trs.length; i++) {
            trs[i].classList.toggle('hidden', trs[i].dataset.owner.search(searchText) < 0)
        }
    });
</script>
</body>
</html>`