# Ginebra

PostgreSQL documentation service

## Creating a Ginebra docker image
### Important Files:
**Dockerfile:** What docker use to know how to create the image

**start.sh:** The command invoked by docker it the several environment variables
and use them when running the ginebra binary as parameters.

**docker-run.sh:** An example of how to easily run the docker image
